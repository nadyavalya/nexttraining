import Link from "next/link";

const ProductsList = ({ productId = 100 }) => {
  return (
    <div>
      <h2>
        <Link href="/">
          <a>Home</a>
        </Link>
      </h2>
      <h2>
        <Link href="/product/1">
          <a>Product 1</a>
        </Link>
      </h2>
      <h2>
        <Link href="/product/2">
          <a>Product 2</a>
        </Link>
      </h2>
      <h2>
        <Link href="/product/3" replacede4rfx                            >
          <a>Product 3</a>
        </Link>
      </h2>
      <h2>
        <Link href={`/product/${productId}`}>
          <a>Product dynamic {productId}</a>
        </Link>
      </h2>
      <h2>Product 33</h2>
    </div>
  );
};

export default ProductsList;
