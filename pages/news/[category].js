const ArticleListByCategory = ({ articles, category }) => {
  return (
    <div>
      <h1>Showing news for category {category}</h1>
      {articles.map((article) => {
        return (
          <div key={article.id}>
            <h2>
              {article.id}
              {article.name}
            </h2>
            <p>{article.url}</p>
            <hr></hr>
          </div>
        );
      })}
    </div>
  );
};

export default ArticleListByCategory;

export async function getServerSideProps(context) {
  const { params, req, res, query } = context;
  const { category } = params;
  console.log(req.headers.cookie);
  console.log(query);
  res.setHeader("Set-Cookie", ["name=Vishwas"]);
  const response = fetch(
    `https://jsonplaceholder.typicode.com/comments?email=${category}`
  );
  const data = await (await response).json();
  return {
    props: {
      articles: data,
      category,
    },
  };
}
