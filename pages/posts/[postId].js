import { useRouter } from "next/router";

function Post({ post }) {
  const router = useRouter();
  if (router.isFallback) {
    return <h2>Is loading</h2>;
  }
  return (
    <div>
      <h2>
        {post.id}
        {post.title}
      </h2>
      <p>{post.body}</p>
    </div>
  );
}
export default Post;

export async function getStaticPaths() {
  //   const response = fetch("https://jsonplaceholder.typicode.com/posts");
  //   const data = await (await response).json();
  //   const paths = data.map((post) => {
  //     return {
  //       params: {
  //         postId: `${post.id}`,
  //       },
  //     };
  //   });
  return {
    // paths,
    paths: [
      {
        params: { postId: "1" },
      },
      {
        params: { postId: "2" },
      },
      {
        params: { postId: "3" },
      },
    ],
    fallback: true,
  };
}

export async function getStaticProps(context) {
  const { params } = context;
  const response = fetch(
    `https://jsonplaceholder.typicode.com/posts/${params.postId}`
  );
  const data = await (await response).json();
  if (!data.id) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      post: data,
    },
    revalidate: 10,
  };
}
