// import { createStore } from "redux";
// import { createWrapper } from "next-redux-wrapper";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import rootReducer from "../reducers";
const initialState = {};

const middleware = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;

// const makeStore = (context) => createStore(rootReducer, initialState);

// export const wrapper = createWrapper(makeStore, { debug: true });
