import { useState, useEffect } from "react";

const Dashboard = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [dashboardData, setDashboardData] = useState(null);
  useEffect(() => {
    async function fetchDashboardData() {
      const response = await fetch(
        "https://jsonplaceholder.typicode.com/comments"
      );
      const data = await response.json();
      data = data.slice(0, 1);
      setDashboardData(data[0]);
      setIsLoading(false);
    }
    fetchDashboardData();
  }, []);
  if (isLoading) {
    return <h2>Loading...</h2>;
  }
  return (
    <div>
      <h2>Dashboard</h2>
      <p>Dashboard id - {dashboardData.id}</p>
      <p>Dashboard name - {dashboardData.name}</p>
      <p>Dashboard id - {dashboardData.email}</p>
    </div>
  );
};

export default Dashboard;
