const User = ({ user }) => {
  return (
    <div>
      <h1>User id{user.id}</h1>
      <p>{user.name}</p>
      <p>{user.email}</p>
    </div>
  );
};

export default User;
