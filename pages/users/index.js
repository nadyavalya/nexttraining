import * as axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect } from "react";
import { setUsersAC } from "../../redux/reducers/users-reducer";
import Row from "../../components/Row";
import TableHeader from "../../components/TableHeader";

const Users = () => {
  const data = useSelector((state) => state.users);
  debugger;

  const dispatch = useDispatch();

  const setData = (users) => {
    dispatch(setUsersAC(users));
  };

  const fetchUsers = async () => {
    const response = await axios
      .get("https://jsonplaceholder.typicode.com/users")
      .catch((err) => {
        console.log("Err", err);
      });
    setData(response.data);
    console.log(response.data);
  };

  useEffect(() => {
    fetchUsers();
  }, []);
  console.log(data);

  //the variable to save all the rows
  let usersData;
  if (data) {
    usersData = data.users.map((user) => {
      return (
        <Row
          key={user.id}
          name={user.name}
          id={user.id}
          phone={user.phone}
          website={user.website}
          city={user.address.city}
          street={user.address.street}
          companyName={user.company.name}
        />
      );
    });
  }

  return (
    <div>
      <table>
        <TableHeader
          id="ID"
          name="Name"
          phone="Phone"
          website="WebSite"
          status="Status"
          city="City"
          street="Street"
          companyName="Company Name"
        />
        {usersData}
      </table>
    </div>
  );
};

export default Users;
