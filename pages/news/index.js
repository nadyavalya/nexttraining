const NewsArticleList = ({ articles }) => {
  return (
    <>
      <h1>List of news articles</h1>
      {articles.map((article) => {
        return (
          <div key={article.id}>
            <h2>
              {article.id}
              {article.name}
              {article.email}
              <br />
            </h2>
          </div>
        );
      })}
    </>
  );
};

export default NewsArticleList;

export async function getServerSideProps() {
  const response = fetch("https://jsonplaceholder.typicode.com/comments");
  const data = await (await response).json();
  return {
    props: {
      articles: data,
    },
  };
}
