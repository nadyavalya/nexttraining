import React from "react";
import PropTypes from "prop-types";
// import "../styles/table.scss";

const Row = (props) => {
  return (
    <tr className={"tableContainer"}>
      <td className={"item"}>{props.id}</td>
      <td className={"item"}>{props.name}</td>
      <td className={"item"}>{props.phone}</td>
      <td className={"item"}>{props.website}</td>
      <td className={"item"}>{props.city}</td>
      <td className={"item"}>{props.street}</td>
      <td className={"item"}>{props.companyName}</td>
    </tr>
  );
};

Row.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string,
  phone: PropTypes.string,
  website: PropTypes.string,
};
export default Row;
