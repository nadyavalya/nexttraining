import Link from "next/link";
import { useRouter } from "next/router";

const Home = (params) => {
  const router = useRouter();
  const handleClick = () => {
    console.log("Placing your order");
    // router.push("/product");
    router.replace("/product");
  };
  return (
    <div>
      <h1>Home page</h1>
      <Link href="/blog">
        <a>Blog</a>
      </Link>
      <Link href="/product">
        <a>Product</a>
      </Link>
      <button onClick={handleClick}>Place order</button>
    </div>
  );
};

export default Home;
