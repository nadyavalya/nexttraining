import Link from "next/link";

const PostsList = ({ posts }) => {
  return (
    <div>
      <h1>List of posts</h1>
      {posts.map((post) => {
        return (
          <div key={post.id}>
            <Link href={`posts/${post.id}`} passHref>
              <h2>
                {post.id}
                {post.title}
              </h2>
            </Link>
            <hr />
            {/* <User post={post} />; */}
          </div>
        );
      })}
    </div>
  );
};

export default PostsList;

export async function getStaticProps() {
  const response = fetch("https://jsonplaceholder.typicode.com/posts");
  const data = await (await response).json();
  return {
    props: {
      //   posts: data,
      posts: data.slice(0, 3),
    },
    revalidate: 10,
  };
}
