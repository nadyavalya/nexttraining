import React from "react";
import PropTypes from "prop-types";
// import "../styles/table.scss";

const TableHeader = (props) => {
  return (
    <tr className={"tableContainer"}>
      <th className={"item"}>{props.id}</th>
      <th className={"item"}>{props.name}</th>
      <th className={"item"}>{props.phone}</th>
      <th className={"item"}>{props.website}</th>
      <th className={"item"}>{props.city}</th>
      <th className={"item"}>{props.street}</th>
      <th className={"item"}>{props.companyName}</th>
    </tr>
  );
};

TableHeader.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  website: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  street: PropTypes.string.isRequired,
  companyName: PropTypes.string.isRequired,
};

export default TableHeader;
