import { useState } from "react";
import { useRouter } from "next/router";

const EventList = ({ eventList }) => {
  const [events, setEvents] = useState(eventList);
  const router = useRouter();

  const fetchedCompletedEvents = async () => {
    const response = fetch(
      "https://jsonplaceholder.typicode.com/todos?completed=true"
    );
    let data = await (await response).json();
    setEvents(data);
    router.push("/events?completed=true", undefined, { shallow: true });
  };
  return (
    <>
      <button onClick={fetchedCompletedEvents}>Completed events</button>
      <h1>List of events</h1>
      {events.map((event) => {
        return (
          <div key={event.id}>
            <h2>
              {event.id}
              {event.title}
              event completed-
              {event.completed}
              <br />
            </h2>
            <hr />
          </div>
        );
      })}
    </>
  );
};

export default EventList;

export async function getServerSideProps(context) {
  const { query } = context;
  const { category } = query;
  const queryString = category ? "completed=true" : "";
  const response = fetch(
    `https://jsonplaceholder.typicode.com/todos?${queryString}`
  );
  let data = await (await response).json();
  data = data.slice(0, 10);
  return {
    props: {
      eventList: data,
    },
  };
}
