import { combineReducers } from "redux";
import formReducer from "./formReducer";
import usersReducer from "./users-reducer";

export default combineReducers({
  form: formReducer,
  users: usersReducer,
});
