import Link from "next/link";
import { useRouter } from "next/router";

const Home = (params) => {
  return (
    <div>
      <h1>Next js prerendering</h1>
      <h2>
        <Link href="/users">
          <a>Home</a>
        </Link>
      </h2>
      <h2>
        <Link href="/posts">
          <a>Posts</a>
        </Link>
      </h2>
    </div>
  );
};

export default Home;
