import useSWR from "swr";

const fetcher = async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/comments");
  const data = await response.json();
  data = data.slice(0, 1);
  return data[0];
};
const DashboardSWR = () => {
  const { data, error } = useSWR("dashboard", fetcher);

  if (error) {
    return <h2>An error has occured</h2>;
  }
  if (!data) {
    return <h2>Loading...</h2>;
  }
  return (
    <div>
      <h2>DashboardSWR</h2>
      <p>Dashboard id - {data.id}</p>
      <p>Dashboard name - {data.name}</p>
      <p>Dashboard id - {data.email}</p>
    </div>
  );
};

export default DashboardSWR;
