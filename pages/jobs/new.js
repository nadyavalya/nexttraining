import React from "react";
import { useForm } from "react-hook-form";
import { connect } from "react-redux";
// import JobPreview from "../../components/JobPreview";
import {
  changeJobTitle,
  changeLocation,
  changeDescription,
} from "../../redux/actions/index";

const JobForm = ({
  changeJobTitle,
  changeLocation,
  changeDescription,
  changeJobType,
}) => {
  const { register, formState: { errors } , handleSubmit } = useForm();

  const onSubmit = async (data) => {
    const fields = { fields: data };
  };

  return (
    <React.Fragment>
      <div className="grid grid-cols-2">
        <div>
          <h1 className="mt-5 text-center text-3xl font-semibold">
            Post a new job
          </h1>
          <form
            className="max-w-2xl m-auto py-10 mt-10 px-12 border"
            onSubmit={handleSubmit(onSubmit)}
          >
            <label className="text-gray-600 font-medium">Job Title</label>
            <input
              className="border-gray-300 border py-1 px-2  w-full rounded text-gray-700"
                name="title"
              autoFocus
              onChange={(e) => changeJobTitle(e.target.value)}
              {...register("title", { required: "Please enter a job title" })}
            />
            {errors.title && (
              <div className="mb-3 text-normal text-red-500 ">
                {errors.title.message}
              </div>
            )}

            <label className="text-gray-600 font-medium block mt-4">
              Location
            </label>
            <input
              className="border-solid border-gray-300 border py-1 px-2 w-full rounded text-gray-700"
              //   name="location"
              type="text"
              placeholder="Scranton, PA"
              onChange={(e) => changeLocation(e.target.value)}
              {...register("location", { required: "Please enter a location" })}
            />
            {errors.location && (
              <div className="mb-3 text-normal text-red-500 ">
                {errors.location.message}
              </div>
            )}

            <label className="text-gray-600 font-medium block mt-4">
              Description
            </label>

            <input
              className="border-solid border-gray-300 border py-1 px-2 w-full rounded text-gray-700"
              //   name="link"
              type="text"
              onChange={(e) => changeDescription(e.target.value)}
              {...register("link", { required: "Please enter a description" })}
            />
            {errors.description && (
              <div className="mb-3 text-normal text-red-500 ">
                {errors.description.message}
              </div>
            )}

            {errors.jobtype && (
              <div className="mb-3 text-normal text-red-500 ">
                {errors.jobtype.message}
              </div>
            )}

            <button
              className="mt-4 w-full bg-green-400 hover:bg-green-600 text-green-100 border py-3 px-6 font-semibold text-md rounded"
              type="submit"
            >
              Submit
            </button>
          </form>
        </div>
        <div>{/* <JobPreview /> */}</div>
      </div>
    </React.Fragment>
  );
};

export default connect((state) => state, {
  changeJobTitle,
  changeLocation,
  changeDescription,
})(JobForm);
