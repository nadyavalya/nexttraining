const SET_USERS = "SET_USERS";

let initialState = {
  users: [],
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USERS:
      return { users: [...action.users] };
    default:
      return state;
  }
};

export default usersReducer;

export const setUsersAC = (users) => {
  return { type: SET_USERS, users };
};
